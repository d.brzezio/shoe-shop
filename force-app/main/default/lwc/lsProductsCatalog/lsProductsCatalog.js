/**
 * Created by damian.brzezinski_en on 12.08.2023.
 */

import { LightningElement, wire } from 'lwc';
import { subscribe, MessageContext } from 'lightning/messageService';
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import LS_MESSAGE_CHANNEL from '@salesforce/messageChannel/LS_MessageChannel__c';

//Apex
import getProducts from '@salesforce/apex/LS_ProductCatalogController.getProducts';
import getNumberOfAllProducts from '@salesforce/apex/LS_ProductCatalogController.getNumberOfAllProducts';

//labels
import ERROR from '@salesforce/label/c.LS_Error';

export default class LsProductsCatalog extends LightningElement {
    isLoading = true;
    products = [];
    numberOfRecordsToGet = 2;
    numberOfRecordsToSkip = 0;
    numberOfAllProducts = 0;
    searchKey;
    minValue;
    maxValue;
    category;
    subscription = null;

    @wire(MessageContext)
    messageContext;

    label = {
        ERROR
    }

    connectedCallback() {
        this.subscribeToMessageChannel();
        this.getNumberOfAllProducts();
        this.getProducts();
    }

    getProducts() {
        let payload = {
            numberOfRecordsToGet: this.numberOfRecordsToGet,
            numberOfRecordsToSkip: this.numberOfRecordsToSkip,
            searchKey: this.searchKey,
            category: this.category,
            minValue: this.minValue,
            maxValue: this.maxValue
        }

        getProducts(payload)
            .then(result => {
                this.products = result;
                this.switchSpinner();
            })
            .catch(error => {
                this.showToast(this.label.ERROR, error, 'error');
            })
    }

    getNumberOfAllProducts() {
        let payload = {
            searchKey: this.searchKey,
            category: this.category,
            minValue: this.minValue,
            maxValue: this.maxValue
        }

        getNumberOfAllProducts(payload)
            .then(result => {
                this.numberOfAllProducts = result;
            })
            .catch(error => {
                this.showToast(this.label.ERROR, error, 'error');
            });
    }

    handleNumberOfProductChange(event) {
        if(event.target.value) {
            this.numberOfRecordsToGet = event.target.value;
            this.numberOfRecordsToSkip = 0;
            this.reloadComponent();
        }
    }

    prevPage(event) {
        if(this.numberOfRecordsToSkip > 0) {
            this.numberOfRecordsToSkip = this.numberOfRecordsToSkip - this.numberOfRecordsToGet;
            this.reloadComponent();
        }
    }

    nextPage(event) {
        if(this.numberOfRecordsToSkip < this.numberOfRecordsToGet * (this.lastPage - 1)) {
            this.numberOfRecordsToSkip = parseInt(this.numberOfRecordsToSkip) + parseInt(this.numberOfRecordsToGet);
            this.reloadComponent();
        }
    }

    subscribeToMessageChannel() {
        this.subscription = subscribe(
            this.messageContext,
            LS_MESSAGE_CHANNEL,
            (message) => this.handleMessage(message)
        );
    }

    handleMessage(message) {
        if(message.details.eventName === 'newSearchKey') {
            this.searchKey = message.details.searchKey;
            this.numberOfRecordsToSkip = 0
            this.getNumberOfAllProducts();
            this.reloadComponent();
        } else if(message.details.eventName === 'newFilters') {
            this.numberOfRecordsToSkip = 0
            this.category = message.details.category;
            this.minValue = message.details.minValue;
            this.maxValue = message.details.maxValue;
            this.getNumberOfAllProducts();
            this.reloadComponent()
        } else if(message.details.eventName === 'refresh') {
            this.reloadComponent();
        }
    }

    reloadComponent() {
        this.switchSpinner();
        this.getProducts();
    }

    switchSpinner() {
        this.isLoading = !this.isLoading;
    }

    showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(evt);
    }

    get actualPage() {
        if(this.lastPage === 0) {
            return 0;
        }
        return parseInt(Number((this.numberOfRecordsToSkip / this.numberOfRecordsToGet) + 1).toString());
    }

    get lastPage() {
        let temp = Number(this.numberOfAllProducts / this.numberOfRecordsToGet);
        if(temp > parseInt(temp.toString())) {
            return parseInt((temp + 1).toString());
        } else {
            return parseInt(temp.toString());
        }
    }
}