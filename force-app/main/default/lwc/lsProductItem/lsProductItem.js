/**
 * Created by damian.brzezinski_en on 14.08.2023.
 */

import {api, LightningElement, wire } from 'lwc';
import { publish, MessageContext } from 'lightning/messageService';
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import LS_MESSAGE_CHANNEL from '@salesforce/messageChannel/LS_MessageChannel__c'

import LS_PRODUCT_ALL_FIELDS from 'c/lsProductUtils';

//Apex
import addProduct from '@salesforce/apex/LS_ProductItemController.addProduct';

//labels
import ERROR from '@salesforce/label/c.LS_Error';
import NAME from '@salesforce/label/c.LS_Name';
import CATEGORY from '@salesforce/label/c.LS_Category';
import PRICE from '@salesforce/label/c.LS_Price';
import DESCRIPTION from '@salesforce/label/c.LS_Description';
import ADD_TO_CART from '@salesforce/label/c.LS_AddToCart';
import OUT_OF_STACK from '@salesforce/label/c.LS_OutOfStack';
import PROD_HAS_BEEN_ADDED from '@salesforce/label/c.LS_ProdHasBeenAdded';

export default class LsProductItem extends LightningElement {
    @api
    record;
    numberOfProducts = 1;

    @wire(MessageContext)
    messageContext;

    label = {
        ERROR,
        NAME,
        CATEGORY,
        PRICE,
        DESCRIPTION,
        ADD_TO_CART,
        OUT_OF_STACK,
        PROD_HAS_BEEN_ADDED
    }

    handleAddToCart(event) {
        addProduct({ productId: this.record.Id, numberOfProducts: this.numberOfProducts })
            .then(result => {
                this.showToast(this.label.ADD_TO_CART, this.label.PROD_HAS_BEEN_ADDED, 'success');
                publish(this.messageContext, LS_MESSAGE_CHANNEL,{ details: { eventName: 'refresh'} });
            }).catch(error => {
                this.showToast(this.label.ERROR, error, 'error');
            });
    }

    handleNumberOfProductsChange(event) {
        this.numberOfProducts = event.target.value;
    }

    showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(evt);
    }

    get name() {
        return this.record[LS_PRODUCT_ALL_FIELDS["Name"]];
    }

    get category() {
        return this.record[LS_PRODUCT_ALL_FIELDS["Category"]];
    }

    get price() {
        return this.record[LS_PRODUCT_ALL_FIELDS["Price"]];
    }

    get img() {
        return this.record[LS_PRODUCT_ALL_FIELDS["ImageUrl"]];
    }

    get description() {
        return this.record[LS_PRODUCT_ALL_FIELDS["Description"]];
    }

    get stock() {
        return this.record[LS_PRODUCT_ALL_FIELDS["Stock"]];
    }

    get noProductAvailable() {
        return this.stock <= 0;
    }

}