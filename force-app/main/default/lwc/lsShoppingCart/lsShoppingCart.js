/**
 * Created by damian.brzezinski_en on 12.08.2023.
 */

import { LightningElement, wire } from 'lwc';
import { deleteRecord, updateRecord } from 'lightning/uiRecordApi';
import { publish, subscribe, MessageContext } from 'lightning/messageService';
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import LS_MESSAGE_CHANNEL from '@salesforce/messageChannel/LS_MessageChannel__c'

import getUserOrderDetails from '@salesforce/apex/LS_ShoppingCartController.getUserOrderDetails';

//labels
import ERROR from '@salesforce/label/c.LS_Error';
import NAME from '@salesforce/label/c.LS_Name';
import QUANTITY from '@salesforce/label/c.LS_Quantity';
import PRICE from '@salesforce/label/c.LS_Price';
import SUM from '@salesforce/label/c.LS_Sum';
import MAKE_AN_ORDER from '@salesforce/label/c.LS_MakeAnOrder';
import ORDER_ITEM from '@salesforce/label/c.LS_OrderItem';
import PROD_DELETED from '@salesforce/label/c.LS_ProdDeletedFromCart';
import ORDER from '@salesforce/label/c.LS_Order';
import ORDER_COMPLETED from '@salesforce/label/c.LS_OrderCompleted';
import MY_CART from '@salesforce/label/c.LS_MyCart';

export default class LsShoppingCart extends LightningElement {

    isLoading = true;
    orderWrapper;

    @wire(MessageContext)
    messageContext;

    subscription = null;

    label = {
        ERROR,
        NAME,
        QUANTITY,
        PRICE,
        SUM,
        MAKE_AN_ORDER,
        ORDER_ITEM,
        PROD_DELETED,
        ORDER,
        ORDER_COMPLETED,
        MY_CART
    }

    connectedCallback() {
        this.subscribeToMessageChannel();
        this.getUserOrderDetails();
    }

    getUserOrderDetails() {
        getUserOrderDetails()
            .then(result => {
                this.orderWrapper = result;
                this.switchSpinner();
            }).catch(error => {
                this.showToast(this.label.ERROR, error, 'error');
            })
    }

    subscribeToMessageChannel() {
        this.subscription = subscribe(
            this.messageContext,
            LS_MESSAGE_CHANNEL,
            (message) => this.handleMessage(message)
        );
    }

    handleMessage(message) {
        if(message.details.eventName === 'refresh') {
            this.reloadComponent();
        }
    }

    handeDeleteRecord(event) {
        deleteRecord(event.target.dataset.id)
            .then(() => {
                this.showToast(this.label.ORDER_ITEM, this.label.PROD_DELETED, 'success');
                publish(this.messageContext, LS_MESSAGE_CHANNEL,{ details: { eventName: 'refresh'} });
            }).catch(error => {
                this.showToast(this.label.ERROR, error, 'error');
            });
    }

    handleMakeAnOrder(event) {
        const fields = {};
        fields['Id'] = this.orderWrapper.orderRecord.Id;
        fields['LS_Status__c'] = 'Done';
        updateRecord( { fields } )
            .then(() => {
                this.showToast(this.label.ORDER, this.label.ORDER_COMPLETED, 'success');
                publish(this.messageContext, LS_MESSAGE_CHANNEL,{ details: { eventName: 'refresh'} });
            }).catch(error => {
                this.showToast(this.label.ERROR, error, 'error');
            });
    }

    reloadComponent() {
        this.switchSpinner();
        this.getUserOrderDetails();
    }

    switchSpinner() {
        this.isLoading = !this.isLoading;
    }

    showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(evt);
    }

    get emptyCart() {
        return this.orderWrapper.orderItemsDetails.length === 0
    }

}