/**
 * Created by damian.brzezinski_en on 14.08.2023.
 */

import {LightningElement, wire} from 'lwc';
import { publish, MessageContext } from 'lightning/messageService';
import LS_MESSAGE_CHANNEL from '@salesforce/messageChannel/LS_MessageChannel__c';

//labels
import PRODUCT_NAME from '@salesforce/label/c.LS_ProductNamePlaceholder';

export default class LsSearchBar extends LightningElement {

    timer;
    searchKey;

    @wire(MessageContext)
    messageContext;

    label = {
        PRODUCT_NAME
    }

    handleTextChange(event) {
        this.searchKey = event.target.value;

        if(this.timer) {
            window.clearTimeout(this.timer);
        }

        this.timer = window.setTimeout(() => {
            this.sendNewSearchKey();
        }, 500);
    }

    sendNewSearchKey() {
        publish(this.messageContext, LS_MESSAGE_CHANNEL, { details: { eventName: 'newSearchKey', searchKey: this.searchKey } });
    }
}