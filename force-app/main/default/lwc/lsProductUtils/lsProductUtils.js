/**
 * Created by damian.brzezinski_en on 14.08.2023.
 */

const LS_PRODUCT_FIELD_NAME = "Name";
const LS_PRODUCT_FIELD_CATEGORY = "LS_Category__c";
const LS_PRODUCT_FIELD_DESCRIPTION = "LS_Description__c";
const LS_PRODUCT_FIELD_EXTERNAL_ID = "LS_ExternalId__c";
const LS_PRODUCT_FIELD_IMAGE_URL = "LS_ImageUrl__c";
const LS_PRODUCT_FIELD_PRICE = "LS_Price__c";
const LS_PRODUCT_FIELD_STOCK = "LS_Stock__c";

const LS_PRODUCT_ALL_FIELDS = {
    "Name" : LS_PRODUCT_FIELD_NAME,
    "Category" : LS_PRODUCT_FIELD_CATEGORY,
    "Description" : LS_PRODUCT_FIELD_DESCRIPTION,
    "ExternalId" : LS_PRODUCT_FIELD_EXTERNAL_ID,
    "ImageUrl" : LS_PRODUCT_FIELD_IMAGE_URL,
    "Price" : LS_PRODUCT_FIELD_PRICE,
    "Stock" : LS_PRODUCT_FIELD_STOCK
}

export default LS_PRODUCT_ALL_FIELDS