/**
 * Created by damian.brzezinski_en on 12.08.2023.
 */

import {LightningElement} from 'lwc';

//labels
import SHOW_FILTERS from '@salesforce/label/c.LS_ShowFilters';
import HIDE_FILTERS from '@salesforce/label/c.LS_HideFilters';

export default class LsSearchProduct extends LightningElement {
    showFilters = false;

    label = {
        SHOW_FILTERS,
        HIDE_FILTERS
    }

    changeFiltersVisibility(event) {
        this.showFilters = !this.showFilters;
    }

    get showFiltersLabel() {
        return this.showFilters ? this.label.HIDE_FILTERS : this.label.SHOW_FILTERS;
    }
}