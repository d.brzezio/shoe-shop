/**
 * Created by damian.brzezinski_en on 14.08.2023.
 */

import {LightningElement, wire} from 'lwc';

import {publish, MessageContext} from 'lightning/messageService';
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import LS_MESSAGE_CHANNEL from '@salesforce/messageChannel/LS_MessageChannel__c';

//apex
import getMinPrice from '@salesforce/apex/LS_ProductFiltersController.getMinProdPrice';
import getMaxPrice from '@salesforce/apex/LS_ProductFiltersController.getMaxProdPrice';
import getCategoryValues from '@salesforce/apex/LS_ProductFiltersController.getCategoryValues';

//labels
import ERROR from '@salesforce/label/c.LS_Error';
import APPLY_FILTERS from '@salesforce/label/c.LS_ApplyFilters';
import RESET_FILTERS from '@salesforce/label/c.LS_ResetFilters';
import CATEGORY from '@salesforce/label/c.LS_Category';
import MIN_PRICE from '@salesforce/label/c.LS_MinPrice';
import MAX_PRICE from '@salesforce/label/c.LS_MaxPrice';

export default class LsProductFilters extends LightningElement {

    minPrice;
    maxPrice;

    selectedMinPrice;
    selectedMaxPrice;

    categoryValues = [];
    actualCategoryValue;

    @wire(MessageContext)
    messageContext;

    label = {
        ERROR,
        APPLY_FILTERS,
        RESET_FILTERS,
        CATEGORY,
        MIN_PRICE,
        MAX_PRICE
    }

    @wire(getMinPrice)
    getMinPrice({ error, data }) {
        if(data) {
            this.minPrice = data;
            this.selectedMinPrice = this.minPrice;
        } else if(error) {
            this.showToast(this.label.ERROR, error, 'error');
        }
    }

    @wire(getMaxPrice)
    getMaxPrice({ error, data }) {
        if(data) {
            this.maxPrice = data;
            this.selectedMaxPrice = this.maxPrice;
        } else if(error) {
            this.showToast(this.label.ERROR, error, 'error');
        }
    }

    @wire(getCategoryValues)
    getCategoryValues({ error, data }) {
        if(data) {
            console.log(data);
            data.forEach((item) => {
                let temp = item.split(';')
                const option = { label: temp[0], value: temp[1] };
                this.categoryValues = [ ...this.categoryValues, option];
            });
        } else if(error) {
            this.showToast(this.label.ERROR, error, 'error');
        }
    }

    handleMinPriceChange(event) {
        this.selectedMinPrice = event.target.value;
    }

    handleMaxPriceChange(event) {
        this.selectedMaxPrice = event.target.value;
    }

    handleChangeCategory(event) {
        this.actualCategoryValue = event.target.value;
    }

    applyFilters(event) {
        let payload = {
            details: {
                eventName: 'newFilters',
                category: this.actualCategoryValue,
                minValue: this.selectedMinPrice,
                maxValue: this.selectedMaxPrice
            }
        }
        publish(this.messageContext, LS_MESSAGE_CHANNEL, payload);
    }

    resetFilters(event) {
        this.actualCategoryValue = null;
        this.selectedMinPrice = this.minPrice;
        this.selectedMaxPrice = this.maxPrice;
        this.applyFilters();
    }

    showToast(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant
        });
        this.dispatchEvent(evt);
    }
}