/**
 * Created by damian.brzezinski_en on 20.08.2023.
 */

trigger LS_OrderItemTrigger on LS_OrderItem__c (after insert, after update, after delete) {
    new LS_TH_OrderItem().execute();
}