/**
 * Created by damian.brzezinski_en on 20.08.2023.
 */

trigger LS_OrderTrigger on LS_Order__c (before delete) {
    new LS_TH_Order().execute();
}