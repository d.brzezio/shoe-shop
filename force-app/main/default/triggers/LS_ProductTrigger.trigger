/**
 * Created by damian.brzezinski_en on 22.08.2023.
 */

trigger LS_ProductTrigger on LS_Product__c (before delete) {
    new LS_TH_Product().execute();
}