/**
 * Created by damian.brzezinski_en on 18.08.2023.
 */

public with sharing class LS_ProductItemController {

    @AuraEnabled
    public static void addProduct(Id productId, Integer numberOfProducts) {
        LS_Order__c orderRecord = getOrder();
        LS_OrderItem__c orderItem = getOrderItem(productId);

        if(String.isBlank(orderItem.Id)) {
            orderItem.LS_ProductId__c = productId;
            orderItem.LS_OrderId__c = orderRecord.Id;
            orderItem.LS_Quantity__c = numberOfProducts;
        } else {
            orderItem.LS_Quantity__c += numberOfProducts;
        }

        upsert orderItem;
    }

    private static LS_Order__c getOrder() {
        List<LS_Order__c> orders = [
                SELECT Id
                FROM LS_Order__c
                WHERE LS_Status__c = 'New' AND OwnerId = :UserInfo.getUserId()
                LIMIT 1
        ];

        if(!orders.isEmpty()) {
            return orders.get(0);
        }

        LS_Order__c orderRecord = new LS_Order__c(
                LS_Status__c = 'New'
        );

        insert orderRecord;

        return orderRecord;
    }

    private static LS_OrderItem__c getOrderItem(Id productId) {
        List<LS_OrderItem__c> orderItems = [
                SELECT Id, LS_ProductId__c, LS_OrderId__c, LS_Quantity__c
                FROM LS_OrderItem__c
                WHERE LS_ProductId__c = :productId AND LS_OrderId__r.LS_Status__c = 'New' AND LS_OrderId__r.OwnerId = :UserInfo.getUserId()
                LIMIT 1
        ];

        return !orderItems.isEmpty() ? orderItems.get(0) : new LS_OrderItem__c();
    }
}