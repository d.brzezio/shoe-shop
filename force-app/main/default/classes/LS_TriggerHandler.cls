/**
 * Created by damian.brzezinski_en on 20.08.2023.
 */

public abstract class LS_TriggerHandler {

    public void execute() {
        if(this.runTrigger()) {
            switch on Trigger.operationType {
                when BEFORE_INSERT {
                    this.beforeInsert(Trigger.new);
                }
                when BEFORE_UPDATE {
                    this.beforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
                }
                when BEFORE_DELETE {
                    this.beforeDelete(Trigger.old, Trigger.oldMap);
                }
                when AFTER_INSERT {
                    this.afterInsert(Trigger.new, Trigger.newMap);
                }
                when AFTER_UPDATE {
                    this.afterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
                }
                when AFTER_DELETE {
                    this.afterDelete(Trigger.old, Trigger.oldMap);
                }
                when AFTER_UNDELETE {
                    this.afterUndelete(Trigger.new, Trigger.newMap);
                }
            }

            finish();
        }
    }

    private Boolean runTrigger() {
        List<LS_TriggerController__mdt> config = [
                SELECT LS_IsActive__c
                FROM LS_TriggerController__mdt
                WHERE DeveloperName = :this.getTriggerName()
        ];

        return config.isEmpty() ? true : config[0].LS_IsActive__c;
    }

    public virtual void beforeInsert(List<SObject> newRecords) {}
    public virtual void beforeUpdate(List<SObject> oldRecords, List<SObject> newRecords, Map<ID, SObject> oldRecordMap, Map<ID, SObject> newRecordMap) {}
    public virtual void beforeDelete(List<SObject> oldRecords, Map<ID, SObject> oldRecordMap) {}
    public virtual void afterInsert(List<SObject> newRecords, Map<ID, SObject> newRecordMap) {}
    public virtual void afterUpdate(List<SObject> oldRecords, List<SObject> newRecords, Map<ID, SObject> oldRecordMap, Map<ID, SObject> newRecordMap) {}
    public virtual void afterDelete(List<SObject> oldRecords, Map<ID, SObject> oldRecordMap) {}
    public virtual void afterUndelete(List<SObject> newRecords, Map<ID, SObject> newRecordMap) {}

    public virtual void finish() {}

    public abstract String getTriggerName();
}