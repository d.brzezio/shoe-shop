/**
 * Created by damian.brzezinski_en on 14.08.2023.
 */

public with sharing class LS_ProductCatalogController {
    @AuraEnabled
    public static List<LS_Product__c> getProducts(Integer numberOfRecordsToGet, Integer numberOfRecordsToSkip, String searchKey, String category, Integer minValue, Integer maxValue) {
        String query = 'SELECT Id, Name, LS_Category__c, LS_ImageUrl__c, LS_Price__c, LS_Stock__c, LS_Description__c FROM LS_Product__c ';

        if(String.isNotBlank(searchKey)) {
            query += 'WHERE Name LIKE \'%' + searchKey + '%\' ';
        }

        if(String.isNotBlank(category)) {
            query = query.contains('WHERE') ? query + 'AND LS_Category__c INCLUDES (\'' + category + '\') ' : query + 'WHERE LS_Category__c INCLUDES (\'' + category + '\') ' ;
        }

        if(minValue != null) {
            query = query.contains('WHERE') ? query + 'AND LS_Price__c >= ' + minValue + ' ' : query + 'WHERE LS_Price__c >= ' + minValue + ' ' ;
        }

        if(maxValue != null) {
            query = query.contains('WHERE') ? query + 'AND LS_Price__c <= ' + maxValue + ' ' : query + 'WHERE LS_Price__c <= ' + maxValue + ' ' ;
        }

        query += 'LIMIT :numberOfRecordsToGet OFFSET :numberOfRecordsToSkip';

        return Database.query(query);
    }

    @AuraEnabled
    public static Integer getNumberOfAllProducts(String searchKey, String category, Integer minValue, Integer maxValue) {
        String query = 'SELECT count() FROM LS_Product__c ';

        if(String.isNotBlank(searchKey)) {
            query += 'WHERE Name LIKE \'%' + searchKey + '%\' ';
        }

        if(String.isNotBlank(category)) {
            query = query.contains('WHERE') ? query + 'AND LS_Category__c INCLUDES (\'' + category + '\') ' : query + 'WHERE LS_Category__c INCLUDES (\'' + category + '\') ' ;
        }

        if(minValue != null) {
            query = query.contains('WHERE') ? query + 'AND LS_Price__c >= ' + minValue + ' ' : query + 'WHERE LS_Price__c >= ' + minValue + ' ' ;
        }

        if(maxValue != null) {
            query = query.contains('WHERE') ? query + 'AND LS_Price__c <= ' + maxValue + ' ' : query + 'WHERE LS_Price__c <= ' + maxValue + ' ' ;
        }

        return Database.countQuery(query);
    }
}