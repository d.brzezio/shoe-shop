/**
 * Created by damian.brzezinski_en on 21.08.2023.
 */

public with sharing class LS_CloseUnresolvedOrdersBatch implements Database.Batchable<sObject>, Schedulable{

    public Database.QueryLocator start(Database.BatchableContext param1) {
        LS_Config__c config = [SELECT LS_NumberOfDaysToDeleteOrder__c FROM LS_Config__c LIMIT 1];
        return Database.getQueryLocator([
                SELECT Id
                FROM LS_Order__c
                WHERE LS_Status__c = 'New' AND CreatedDate <= :Date.today().addDays(-((Integer) config.LS_NumberOfDaysToDeleteOrder__c))
        ]);
    }
    public void execute(Database.BatchableContext param1, List<sObject> param2) {
        delete (List<LS_Order__c>) param2;
    }

    public void finish(Database.BatchableContext param1) {
    }

    public void execute(SchedulableContext param1) {
        Database.executeBatch(new LS_CloseUnresolvedOrdersBatch());
    }
}