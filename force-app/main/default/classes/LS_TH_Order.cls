/**
 * Created by damian.brzezinski_en on 20.08.2023.
 */

public with sharing class LS_TH_Order extends LS_TriggerHandler {

    public override void beforeDelete(List<SObject> oldRecords, Map<ID, SObject> oldRecordMap) {
        List<LS_OrderItem__c> ordersToDelete = [
                SELECT Id
                FROM LS_OrderItem__c
                WHERE LS_OrderId__c IN :oldRecordMap.keySet()
        ];

        delete ordersToDelete;
    }

    public override String getTriggerName() {
        return 'LS_TH_Order';
    }
}