/**
 * Created by damian.brzezinski_en on 20.08.2023.
 */

public with sharing class LS_ProductFiltersController {

    @AuraEnabled(cacheable=true)
    static public Long getMinProdPrice() {
        return ((Decimal) ([SELECT MIN(LS_Price__c) FROM LS_Product__c][0].get('expr0'))).round(System.RoundingMode.DOWN);
    }

    @AuraEnabled(cacheable=true)
    static public Long getMaxProdPrice() {
        return ((Decimal) ([SELECT MAX(LS_Price__c) FROM LS_Product__c][0].get('expr0'))).round(System.RoundingMode.CEILING);
    }

    @AuraEnabled(cacheable=true)
    static public List<String> getCategoryValues() {
        List<String> result = new List<String>();

        Schema.DescribeFieldResult categoryDescribe = LS_Product__c.LS_Category__c.getDescribe();
        List<Schema.PicklistEntry> picklistValues = categoryDescribe.getPicklistValues();

        for(Schema.PicklistEntry entry : picklistValues) {
            result.add(entry.getLabel() + ';' + entry.getValue());
        }

        return result;
    }
}