/**
 * Created by damian.brzezinski_en on 20.08.2023.
 */

public with sharing class LS_TH_OrderItem extends LS_TriggerHandler{

    List<LS_Product__c> productsToUpdate = new List<LS_Product__c>();

    public override void afterInsert(List<SObject> newRecords, Map<ID, SObject> newRecordMap) {
        Map<Id, Decimal> productIdToChange = new Map<Id, Decimal>();

        for(LS_OrderItem__c item : (List<LS_OrderItem__c>) newRecords) {
            if(productIdToChange.containsKey(item.LS_ProductId__c)) {
                productIdToChange.put(item.LS_ProductId__c, productIdToChange.get(item.LS_ProductId__c) - item.LS_Quantity__c);
            } else {
                productIdToChange.put(item.LS_ProductId__c, -item.LS_Quantity__c);
            }
        }

        changeProductQuantity(productIdToChange);
    }

    public override void afterUpdate(List<SObject> oldRecords, List<SObject> newRecords, Map<ID, SObject> oldRecordMap, Map<ID, SObject> newRecordMap) {
        Map<Id, LS_OrderItem__c> oldMap = (Map<Id, LS_OrderItem__c>) oldRecordMap;

        Map<Id, Decimal> productIdToChange = new Map<Id, Decimal>();

        for(LS_OrderItem__c item : (List<LS_OrderItem__c>) newRecords) {
            Decimal change = oldMap.get(item.Id).LS_Quantity__c - item.LS_Quantity__c;
            if(productIdToChange.containsKey(item.LS_ProductId__c)) {
                productIdToChange.put(item.LS_ProductId__c, productIdToChange.get(item.LS_ProductId__c) + change);
            } else {
                productIdToChange.put(item.LS_ProductId__c, change);
            }
        }

        changeProductQuantity(productIdToChange);
    }

    public override void afterDelete(List<SObject> oldRecords, Map<ID, SObject> oldRecordMap) {
        Map<Id, Decimal> productIdToChange = new Map<Id, Decimal>();

        for(LS_OrderItem__c item : (List<LS_OrderItem__c>) oldRecords) {
            if(productIdToChange.containsKey(item.LS_ProductId__c)) {
                productIdToChange.put(item.LS_ProductId__c, productIdToChange.get(item.LS_ProductId__c) + item.LS_Quantity__c);
            } else {
                productIdToChange.put(item.LS_ProductId__c, item.LS_Quantity__c);
            }
        }

        changeProductQuantity(productIdToChange);
    }

    public override void finish() {
        if(!productsToUpdate.isEmpty()) {
            update productsToUpdate;
        }
    }

    public override String getTriggerName() {
        return 'LS_TH_OrderItem';
    }

    void changeProductQuantity(Map<Id, Decimal> productIdToChange) {
        productsToUpdate.addAll([
                SELECT Id, LS_Stock__c
                FROM LS_Product__c
                WHERE Id IN :productIdToChange.keySet()
        ]);

        for(LS_Product__c product : productsToUpdate) {
            product.LS_Stock__c = product.LS_Stock__c + productIdToChange.get(product.Id);
        }
    }
}