/**
 * Created by damian.brzezinski_en on 22.08.2023.
 */

public with sharing class LS_TH_Product extends LS_TriggerHandler{

    public override void beforeDelete(List<SObject> oldRecords, Map<ID, SObject> oldRecordMap) {
        checkIfProductsCanBeDeleted((Map<Id, LS_Product__c>) oldRecordMap);
    }

    private void checkIfProductsCanBeDeleted(Map<Id, LS_Product__c> productMap) {

        List<LS_OrderItem__c> items = [
                SELECT Id, LS_ProductId__c
                FROM LS_OrderItem__c
                WHERE LS_ProductId__c IN :productMap.keySet()
        ];

        for(LS_OrderItem__c item : items) {
            productMap.get(item.LS_ProductId__c).addError(Label.LS_DeleteProdError.replace('{0}', item.LS_ProductId__c).replace('{1}', item.Id));
        }
    }

    public override String getTriggerName() {
        return 'LS_TH_Product';
    }
}