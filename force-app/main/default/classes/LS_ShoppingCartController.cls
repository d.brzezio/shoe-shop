/**
 * Created by damian.brzezinski_en on 20.08.2023.
 */

public with sharing class LS_ShoppingCartController {

    @AuraEnabled
    public static OrderWrapper getUserOrderDetails() {
        OrderWrapper ow = new OrderWrapper(UserInfo.getUserId());
        return ow.orderRecord == null ? null : ow;
    }


    public class OrderWrapper {

        @AuraEnabled
        public LS_Order__c orderRecord;

        @AuraEnabled
        public List<LS_OrderItem__c> orderItemsDetails;

        public OrderWrapper(Id userId) {
            List<LS_Order__c> orderDetails = [
                    SELECT Id, LS_Status__c, LS_Sum__c, (SELECT Id, Name, LS_ProductId__r.Name, LS_ProductId__r.LS_Price__c, LS_ProductId__r.LS_ImageUrl__c, LS_Quantity__c, LS_Sum__c FROM Order_Items__r)
                    FROM LS_Order__c
                    WHERE LS_Status__c = 'New' AND OwnerId = :userId
            ];

            if(!orderDetails.isEmpty()) {
                this.orderRecord = orderDetails[0];
                this.orderItemsDetails = orderDetails[0].Order_Items__r;
            }
        }

    }


}